import pytest
from main import app

@pytest.fixture
def client():
    # Create a test client for the Flask app
    client = app.test_client()
    yield client

def test_hello_world(client):
    # Send a GET request to the root URL
    response = client.get('/')
    
    # Check if the response status code is 200 (OK)
    assert response.status_code == 200

    # Check if the response data contains the expected text
    assert b'Hello, World!' in response.data

if __name__ == '__main__':
    pytest.main()

